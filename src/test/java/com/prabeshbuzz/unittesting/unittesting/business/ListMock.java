package com.prabeshbuzz.unittesting.unittesting.business;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.ArgumentCaptor;


import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ListMock {
	
	List mockList = Mockito.mock(List.class);

	@Test
	public void test() {
		
		when(mockList.size()).thenReturn(5);
		assertEquals(5,mockList.size());
	}
	
	@Test
	public void testmultiple() {
		when(mockList.size()).thenReturn(5).thenReturn(6);
		assertEquals(5,mockList.size());
		assertEquals(6,mockList.size());
		}
	
	@Test
	public void returnWithGenericParameter() {
		when(mockList.get(anyInt())).thenReturn("hey its any value");
		assertEquals("hey its any value",mockList.get(0));
		//assertEquals(6,mock.get(0));
		
	}
	
	
	@Test
	public void argumentMatching() {
		
		mockList.add("SomeString");
		
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(mockList).add(captor.capture());
		assertEquals("SomeString", captor.getValue());
		
	}
}
