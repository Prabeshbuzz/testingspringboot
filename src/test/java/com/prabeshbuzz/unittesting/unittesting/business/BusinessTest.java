package com.prabeshbuzz.unittesting.unittesting.business;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BusinessTest {

	@Test
	void CalculateSum_basic() {
		BusinessImpl business = new BusinessImpl();
		int actualresult = business.calculateSum(new int[] {1,2,2});
		int expectedresult = 5;
		assertEquals(expectedresult, actualresult);
	}
	
	@Test
	void CalculateSum_empty() {
		BusinessImpl business = new BusinessImpl();
		int actualresult = business.calculateSum(new int[] {});
		int expectedresult = 0;
		assertEquals(expectedresult, actualresult);
	}

}
