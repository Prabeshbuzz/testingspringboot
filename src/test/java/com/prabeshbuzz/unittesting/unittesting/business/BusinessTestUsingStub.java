package com.prabeshbuzz.unittesting.unittesting.business;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.prabeshbuzz.unittesting.unittesting.data.SomeDataService;

class BusinessStub implements SomeDataService {

	@Override
	public int[] getAllData() {

		return new int[] { 1, 2, 2 };
	}

}

class BusinessTestUsingStub {

	@Test
	void CalculateSum_basicStub() {
		BusinessImpl business = new BusinessImpl();
		business.setSomeDataService(new BusinessStub());
		int actualresult = business.calculateSumUsingDataService();
		int expectedresult = 5;
		assertEquals(expectedresult, actualresult);
	}

	@Test
	void CalculateSum_emptyStub() {
		BusinessImpl business = new BusinessImpl();
		int actualresult = business.calculateSumUsingDataService();
		int expectedresult = 0;
		assertEquals(expectedresult, actualresult);
	}

}
