package com.prabeshbuzz.unittesting.unittesting.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.prabeshbuzz.unittesting.unittesting.data.SomeDataService;

@RunWith(SpringRunner.class)
@SpringBootTest
class BusinessTestUsingMock {

	@InjectMocks
	BusinessImpl business ;
	
	@Mock
	SomeDataService dataService ;

	@Test
	void CalculateSum_basicStub() {

		when(dataService.getAllData()).thenReturn(new int[] { 1, 2, 3 });
		int actualresult = business.calculateSumUsingDataService();
		int expectedresult = 6;
		assertEquals(expectedresult, actualresult);
	}

	@Test
	void CalculateSum_emptyStub() {

		when(dataService.getAllData()).thenReturn(new int[] {});
		int actualresult = business.calculateSumUsingDataService();
		int expectedresult = 0;
		assertEquals(expectedresult, actualresult);
	}

}
