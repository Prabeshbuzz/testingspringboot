package com.prabeshbuzz.unittesting.unittesting.spike;

import static org.junit.jupiter.api.Assertions.*;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

class JsonAssertTest {

	String jsonresult = "{\"id\":1,\"name\":\"Ball\",\"price\":10,\"quantity\":20}";
	@Test
	void jsonAsserttest() throws JSONException {
		String expectedresult = "{\"id\":1,\"name\":\"Ball\",\"price\":10}";
		JSONAssert.assertEquals(expectedresult, jsonresult, false);
	}

}
