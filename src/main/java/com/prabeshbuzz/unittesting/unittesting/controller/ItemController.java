package com.prabeshbuzz.unittesting.unittesting.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prabeshbuzz.unittesting.unittesting.model.item;

@RestController
public class ItemController {
	
	@GetMapping("getitem")
	public item getitem() {
		return new item(1,"Ball",10,20);
	}
}

