package com.prabeshbuzz.unittesting.unittesting.business;

import com.prabeshbuzz.unittesting.unittesting.data.SomeDataService;

public class BusinessImpl {
	
	private SomeDataService SomeDataService;
	
	
	public void setSomeDataService(SomeDataService someDataService) {
		SomeDataService = someDataService;
	}

	public int calculateSum (int[] data) {
		int sum = 0;
		for (int value : data) {
			sum += value;
		}
			return sum;
		}
	
	public int calculateSumUsingDataService() {
		int sum = 0;
		int data[]  = SomeDataService.getAllData();
		for (int value : data) {
			sum += value;
		}
			return sum;
		}
	}
 
